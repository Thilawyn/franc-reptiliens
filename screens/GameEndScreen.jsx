import { useNavigation } from "@react-navigation/native"
import React from "react"
import { Image, ScrollView, View } from "react-native"
import { Avatar, Button, Card, Headline, Paragraph, Text, Title } from "react-native-paper"
import { useRoom } from "../contexts/RoomContext"
import { useUser } from "../contexts/UserContext"


export default function GameEndScreen(props) {

    const navigation = useNavigation()

    const [ user ]  = useUser()
    const [ room ]  = useRoom()
    const { state } = room

    const groupFound = state.sortedChoosedGroups[0] === state.conspiratorGroup
    const plotFound = (
        state.sortedChoosedPlots[0].name === state.plot.name ||
        state.sortedChoosedPlots[1].name === state.plot.name
    )


    return (
        <ScrollView contentContainerStyle={{ padding: 20 }}>
            <Card elevation={4}>
                <Card.Title
                    title={(
                        plotFound && groupFound ?
                            "Gagné !"
                        :
                            plotFound || groupFound ?
                                "(Presque) gagné !"
                            :
                                "Perdu !"
                    )}

                    subtitle={(
                        plotFound && groupFound ?
                            "Vous avez trouvé le complot et les conspirateurs !"
                        :
                            groupFound ?
                                "Vous avez trouvé les conspirateurs, mais pas le complot !"
                            :
                                plotFound ?
                                    "Vous avez trouvé le complot, mais pas les conspirateurs !"
                                :
                                    "Vous n'avez trouvé ni le complot, ni les conspirateurs !"
                    )}

                    left={props => <Avatar.Icon {...props} icon="emoticon-cool" />}
                />

                <Card.Content>
                    <View
                        style={{
                            margin: 10,
                        }}
                    >
                        <Title
                            style={{
                                textAlign: "center",
                                marginBottom: 12,
                            }}
                        >
                            Le complot
                        </Title>

                        <Text
                            style={{
                                textAlign: "center",
                                marginBottom: 6,
                            }}
                        >
                            Les { state.conspiratorGroup } se sont organisés pour mettre à exécution leur terrible dessein :
                        </Text>

                        <Headline style={{ textAlign: "center" }}>{ state.plot.name }</Headline>

                        <Image
                            style={{
                                width: 250,
                                height: 250,

                                margin: 10,
                                marginLeft: "auto",
                                marginRight: "auto",
                            }}
                            source={{ uri: state.plot.imageURL }}
                        />
                    </View>

                    <View
                        style={{
                            margin: 10,
                        }}
                    >
                        <Title
                            style={{
                                textAlign: "center",
                                marginBottom: 12,
                            }}
                        >
                            Les résultats de votre enquête
                        </Title>

                        <Text
                            style={{
                                textAlign: "center",
                            }}
                        >
                            Premier choix de conspirateurs :
                        </Text>
                        <Headline
                            style={{
                                textAlign: "center",
                                marginBottom: 6,
                            }}
                        >
                            { state.sortedChoosedGroups[0] }
                        </Headline>
                        <Text
                            style={{
                                textAlign: "center",
                                marginBottom: 24,
                            }}
                        >
                            Second choix : { state.sortedChoosedGroups[1] }
                        </Text>

                        <Text
                            style={{
                                textAlign: "center",
                                marginBottom: 6,
                            }}
                        >
                            Votre top 2 des choix de complots (une occurence du complot est suffisante pour le trouver) :
                        </Text>
                        <Title
                            style={{
                                textAlign: "center",
                            }}
                        >
                            { state.sortedChoosedPlots[0].name }
                        </Title>
                        <Text
                            style={{
                                textAlign: "center",
                            }}
                        >
                            et
                        </Text>
                        <Title
                            style={{
                                textAlign: "center",
                                marginBottom: 6,
                            }}
                        >
                            { state.sortedChoosedPlots[1].name }
                        </Title>
                    </View>
                </Card.Content>

                <Card.Actions>
                    <Button
                        mode="contained"
                        onPress={() => {
                            navigation.navigate("Room")
                        }}
                    >
                        Revenir
                    </Button>
                </Card.Actions>
            </Card>
        </ScrollView>
    )

}
