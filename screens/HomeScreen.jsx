import { useNavigation } from "@react-navigation/native"
import React, { useEffect, useState } from "react"
import { StyleSheet, View } from "react-native"
import { Avatar, Button, Card, Paragraph, Snackbar, TextInput } from "react-native-paper"
import UserBar from "../components/user/UserBar"
import { useRoom } from "../contexts/RoomContext"
import { useSocket } from "../contexts/SocketContext"
import { useUser } from "../contexts/UserContext"


const styles = StyleSheet.create({
    view: {
        padding: 20,
    },

    card: {
        marginBottom: 10,
    },
})


export default function HomeScreen(props) {

    const navigation = useNavigation()

    const [user, setUser] = useUser()
    const [room, setRoom] = useRoom()

    const [roomID, setRoomID] = useState("")
    const [error, setError] = useState(null)


    const [socket] = useSocket()

    useEffect(() => {
        if (!user)
            navigation.navigate("User", { goTo: "Home" })


        function onRoomJoined(data) {
            const { room } = data
            console.log("Room " + room.id + " joined.")

            setRoom(room)
            navigation.navigate("Room")
        }
        socket.on("room.joined", onRoomJoined)

        function onRoomUnjoinable() {
            setError("Impossible de rejoindre la salle demandée.")
        }
        socket.on("room.unjoinable", onRoomUnjoinable)


        return () => {
            socket.off("room.joined", onRoomJoined)
            socket.off("room.unjoinable", onRoomUnjoinable)
        }
    }, [])


    return <>
        <UserBar />

        <View style={styles.view}>
            <Card
                style={styles.card}
                elevation={4}
            >
                <Card.Title
                    title="Rejoindre une partie"
                    left={props => <Avatar.Icon {...props} icon="account-arrow-right" />}
                />

                <Card.Content>
                    <Paragraph>
                        Rejoignez la partie hébergée par votre partenaire.
                    </Paragraph>

                    <TextInput
                        mode="outlined"
                        label="ID de la salle"
                        value={roomID}
                        onChangeText={newRoomID => setRoomID(newRoomID)}
                    />
                </Card.Content>

                <Card.Actions>
                    <Button
                        mode="text"

                        onPress={() => {
                            socket.emit("room.join", { user, roomID })
                        }}
                    >
                        Rejoindre
                    </Button>
                </Card.Actions>
            </Card>

            <Card
                style={styles.card}
                elevation={4}
            >
                <Card.Title
                    title="Héberger une partie"
                    left={props => <Avatar.Icon {...props} icon="access-point-network" />}
                />

                <Card.Content>
                    <Paragraph>
                        Hébergez une partie et invitez un partenaire à vous rejoindre.
                    </Paragraph>
                </Card.Content>

                <Card.Actions>
                    <Button
                        mode="text"

                        onPress={() => {
                            socket.emit("room.create", { user })
                        }}
                    >
                        Héberger
                    </Button>
                </Card.Actions>
            </Card>
        </View>

        <Snackbar
            visible={Boolean(error)}
            onDismiss={() => setError(null)}
            duration={3000}
        >
            {error}
        </Snackbar>
    </>

}
