import React from "react"
import { View } from "react-native"
import { Avatar, Card, Text } from "react-native-paper"


export default function TurnPassScreen(props) {

    return (
        <View style={{ padding: 20 }}>
            <Card elevation={4}>
                <Card.Title
                    title="Repos !"
                    subtitle="Hacker"
                    left={props => <Avatar.Icon {...props} icon="emoticon-cool" />}
                />

                <Card.Content>
                    <Text>Vous n'avez rien à faire pendant ce tour. Veuillez attendre que votre partenaire joue.</Text>
                    <Text>Restez à l'écoute : ce dernier peut vous demander votre avis sur le choix qu'il a à faire !</Text>
                </Card.Content>
            </Card>
        </View>
    )

}
