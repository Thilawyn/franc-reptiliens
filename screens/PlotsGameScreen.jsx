import React, { useState } from "react"
import { ScrollView, View } from "react-native"
import { Avatar, Button, Card, Checkbox, Text } from "react-native-paper"
import { useRoom } from "../contexts/RoomContext"
import { useSocket } from "../contexts/SocketContext"
import { useUser } from "../contexts/UserContext"


export default function PlotsGameScreen(props) {

    const [ user ]   = useUser()
    const [ socket ] = useSocket()
    const [ room ]   = useRoom()
    const { state }  = room

    const [choosedPlots, setChoosedPlots] = useState([])


    return (
        <ScrollView
            contentContainerStyle={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "stretch",
            }}
        >
            <Card
                elevation={4}
                style={{ margin: 20 }}
            >
                <Card.Title
                    title="Réfléchissez"
                    subtitle="Investigateur"
                    left={props => <Avatar.Icon {...props} icon="magnify" />}
                />

                <Card.Content>
                    <Text>Tout au long de votre carrière, vous vous êtes trouvé sur le chemin de nombreuses conspirations.</Text>
                    <Text>Maintenant que vous avez désormais en tête quelques potentiels groupes de conspirateurs, choisissez 2 complots vous paraissant les plus plausibles.</Text>
                    <Text>Vous pouvez demander l'avis de votre partenaire.</Text>
                </Card.Content>
            </Card>

            <View
                style={{
                    margin: 20,

                    display: "flex",
                    flexDirection: "column",
                    alignItems: "stretch",
                }}
            >
                {state.turnPlots.map(el =>
                    <Card
                        key={el.name}
                        style={{ marginBottom: 6 }}
                    >
                        <Card.Content
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "flex-start",
                                alignItems: "center",
                            }}
                        >
                            <Checkbox
                                status={choosedPlots.includes(el) ? "checked" : "unchecked"}
                                onPress={() => {
                                    setChoosedPlots(
                                        choosedPlots.includes(el) ?
                                            choosedPlots.filter(conspirator => conspirator !== el)
                                        :
                                            choosedPlots.length < 2 ?
                                                [...choosedPlots, el]
                                            :
                                                choosedPlots
                                    )
                                }}
                            />

                            <Text style={{ marginLeft: 2 }}>{ el.name }</Text>
                        </Card.Content>
                    </Card>
                )}
            </View>

            <Button
                mode="contained"
                style={{
                    marginLeft: "auto",
                    marginRight: "auto",
                    marginBottom: 20,
                }}

                disabled={choosedPlots.length < 2}
                onPress={() => {
                    choosedPlots.length === 2 &&
                        socket.emit("room.investigatorPlayed", { roomID: room.id, choosedPlots })
                }}
            >
                Valider
            </Button>
        </ScrollView>
    )

}
