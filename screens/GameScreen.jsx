import { useNavigation } from "@react-navigation/native"
import React, { useEffect } from "react"
import { useRoom } from "../contexts/RoomContext"
import { useUser } from "../contexts/UserContext"
import GroupsGameScreen from "./GroupsGameScreen"
import HackerGameScreen from "./HackerGameScreen"
import PlotsGameScreen from "./PlotsGameScreen"
import TurnPassScreen from "./TurnPassScreen"


export default function GameScreen(props) {

    const navigation = useNavigation()

    const [ user ]  = useUser()
    const [ room ]  = useRoom()
    const { state } = room

    useEffect(() => {
        if (!room) {
            navigation.navigate("Home")
            return
        }

        if (state.done)
            navigation.navigate("GameEnd")
    }, [ room ])


    const Screen = state.investigatorUser.id === user.id ?
        state.investigatorJob === "groups" ?
            GroupsGameScreen
        :
            PlotsGameScreen
    :
        state.investigatorJob === "groups" ?
            HackerGameScreen
        :
            TurnPassScreen


    return <>
        { Screen && <Screen /> }
    </>

}
