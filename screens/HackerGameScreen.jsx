import React from "react"
import { Image, StyleSheet, View } from "react-native"
import { Avatar, Card, Text } from "react-native-paper"
import { useRoom } from "../contexts/RoomContext"
import { useSocket } from "../contexts/SocketContext"
import { useUser } from "../contexts/UserContext"


const styles = StyleSheet.create({
    view: {
        padding: 20,
    },

    cards: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        flexWrap: "wrap",
    },

    card: {
        margin: 6,
    },

    image: {
        width: 100,
        height: 100,
    },
})


export default function HackerGameScreen(props) {

    const [ user ]   = useUser()
    const [ socket ] = useSocket()
    const [ room ]   = useRoom()
    const { state }  = room


    return (
        <View style={styles.view}>
            <Card elevation={4}>
                <Card.Title
                    title="Décrivez ce que vous voyez"
                    subtitle="Hacker"
                    left={props => <Avatar.Icon {...props} icon="emoticon-cool" />}
                />

                <Card.Content>
                    <Text>En fouillant les serveurs AWS à l'aide d'une faille dans Log4J, vous trouvez une mailing list impliquant les personnes et les organisations suivantes.</Text>
                    <Text>Trouvez leur nom et communiquez-les à votre partenaire.</Text>
                </Card.Content>
            </Card>

            <View style={styles.cards}>
                {state.turnConspirators.map(el =>
                    <Card
                        key={el.name}
                        style={styles.card}
                    >
                        <Card.Content>
                            <Image
                                style={styles.image}
                                source={{ uri: el.imageURL }}
                            />
                        </Card.Content>
                    </Card>
                )}
            </View>
        </View>
    )

}
