import { useNavigation } from "@react-navigation/native"
import React, { useEffect } from "react"
import { BackHandler, StyleSheet, View } from "react-native"
import { Avatar, Button, Card, List } from "react-native-paper"
import { useRoom } from "../contexts/RoomContext"
import { useSocket } from "../contexts/SocketContext"
import { useUser } from "../contexts/UserContext"


const styles = StyleSheet.create({
    view: {
        padding: 20,
    },
})


export default function RoomScreen(props) {

    const navigation = useNavigation()

    const [user] = useUser()
    const [socket] = useSocket()
    const [room] = useRoom()


    if (!room) return <></>

    const isLeader = room.leader.id === user.id


    useEffect(() => {
        const handler = BackHandler.addEventListener("hardwareBackPress", () => {
            socket.emit("room.leave", { user, roomID: room.id })
        })

        return () => handler.remove()
    }, [])

    useEffect(() => {
        function onRoomStarted() {
            navigation.navigate("GameStart")
        }
        socket.on("room.started", onRoomStarted)

        function onRoomClosed() {
            navigation.navigate("Home")
        }
        socket.on("room.closed", onRoomClosed)


        return () => {
            socket.off("room.started", onRoomStarted)
            socket.off("room.closed", onRoomClosed)
        }
    }, [socket])


    return (
        <View style={styles.view}>
            <Card elevation={4}>
                <Card.Title
                    title={"Salle n°" + room.id}
                    left={props => <Avatar.Icon {...props} icon="server" />}
                />

                <Card.Content>
                    <List.Section title="Joueurs">
                        {room.users.map(roomUser =>
                            <List.Item
                                key={roomUser.id}
                                left={props => <List.Icon {...props} icon="account" />}
                                title={roomUser.name}
                                description={room.leader.id === roomUser.id ? "Leader" : ""}
                            />
                        )}
                    </List.Section>
                </Card.Content>

                <Card.Actions>
                    {isLeader && room.users.length === 2 &&
                        <Button
                            mode="contained"
                            onPress={() => {
                                socket.emit("room.start", { roomID: room.id })
                            }}
                        >
                            Lancer
                        </Button>
                    }

                    <Button
                        mode="text"
                        onPress={() => {
                            socket.emit("room.leave", { user, roomID: room.id })
                            navigation.goBack()
                        }}
                    >
                        Quitter
                    </Button>
                </Card.Actions>
            </Card>
        </View>
    )

}
