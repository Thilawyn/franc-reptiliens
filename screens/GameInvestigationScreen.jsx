import { useNavigation } from "@react-navigation/native"
import React from "react"
import { StyleSheet, View, Image } from "react-native"
import { Avatar, Button, Card, List, Paragraph, Text, Title } from "react-native-paper"
import UserBar from "../components/user/UserBar"
import { useUser } from "../contexts/UserContext"
import { Dimensions } from "react-native"

const {width, height} = Dimensions.get('window')

//objet JSON pour test
const DATA = [
    {
      partie: 'instigator',
      choices: [
          'Anne Hidalgo',
          'La Confrérie des Francs-Reptiliens',
          'Le partis communiste'
        ],
    },
    {
      partie: 'objective',
      choices: [
        'veut contrôler',
        'veut détruire',
        'veut transformer'
      ],
    },
    {
      partie: 'target',
      choices: [
        'La mairie de Paris',
        'Le monde',
        'La Chine'
      ],
    },
  ];

const styles = StyleSheet.create({
    view: {
        padding: 20,
    },

    card: {
        marginBottom: 10,
    },
    investigationElementList: {
        marginTop: 10,
        flexDirection: "column",
        alignSelf: "flex-start",
    },
    investigationElement: {
        height: height/12,
        width: width/1.4,
        marginBottom: 5,
        paddingBottom: "5%",
    },
})


export default function GameInvestigationScreen(props) {

    const navigation = useNavigation()

    const [user, setUser] = useUser()

    //à synchroniser avec les indices
    //choix de la phase à faire progresser selon la partie
    const phase = 0
    //un choisis de façon prédeterminé à chaque partie, le reste random à chaque phase
    const choix1 = 0
    const choix2 = 1
    const choix3 = 2

    return <>
        <UserBar />

        <View style={styles.view}>
            <Card
                style={styles.card}
                elevation={4}
            >
                <Card.Title
                    title="Mode Enquête"
                />

                <Card.Content>
                    <Paragraph>
                        Trouvez le complot à l'aide de votre partenaire.
                    </Paragraph>
                    <Text>
                        Trouvez l'instigateur du complot :
                    </Text>
                    <View style={styles.investigationElementList}>
                        <Card style={styles.investigationElement}>
                            <Card.Content>
                                <Text>
                                    {/*Faire une action on click*/}
                                    {DATA[phase].choices[choix1]}
                                </Text>
                            </Card.Content>
                        </Card>
                        <Card style={styles.investigationElement}>
                            <Card.Content>
                                <Text>
                                    {/*Faire une action on click*/}
                                    {DATA[phase].choices[choix2]}
                                </Text>
                            </Card.Content>
                        </Card>
                        <Card style={styles.investigationElement}>
                            <Card.Content>
                                <Text>
                                    {/*Faire une action on click*/}
                                    {DATA[phase].choices[choix3]}
                                </Text>
                            </Card.Content>
                        </Card>
                    </View>
                </Card.Content>
            </Card>
        </View>
    </>

}
