import React, { useState } from "react"
import { View } from "react-native"
import { Avatar, Button, Card, Checkbox, Text } from "react-native-paper"
import { useRoom } from "../contexts/RoomContext"
import { useSocket } from "../contexts/SocketContext"
import { useUser } from "../contexts/UserContext"


export default function GroupsGameScreen(props) {

    const [ user ]   = useUser()
    const [ socket ] = useSocket()
    const [ room ]   = useRoom()
    const { state }  = room

    const [choosedGroups, setChoosedGroups] = useState([])


    return (
        <View style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "stretch",
        }}>
            <Card
                elevation={4}
                style={{ margin: 20 }}
            >
                <Card.Title
                    title="Écoutez et analysez"
                    subtitle="Investigateur"
                    left={props => <Avatar.Icon {...props} icon="magnify" />}
                />

                <Card.Content>
                    <Text>Vous devez affiner vos études pour tenter de trouver l'auteur du complot.</Text>
                    <Text>Votre partenaire va vous donner le nom d'un certain nombre de personnes et d'organisations.</Text>
                    <Text>Choisissez 2 groupes les décrivant le mieux.</Text>
                </Card.Content>
            </Card>

            <View
                style={{
                    margin: 20,

                    display: "flex",
                    flexDirection: "column",
                    alignItems: "stretch",
                }}
            >
                {state.allGroups.map(el =>
                    <Card
                        key={el}
                        style={{ marginBottom: 6 }}
                    >
                        <Card.Content
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "flex-start",
                                alignItems: "center",
                            }}
                        >
                            <Checkbox
                                status={choosedGroups.includes(el) ? "checked" : "unchecked"}
                                onPress={() => {
                                    setChoosedGroups(
                                        choosedGroups.includes(el) ?
                                            choosedGroups.filter(conspirator => conspirator !== el)
                                        :
                                            choosedGroups.length < 2 ?
                                                [...choosedGroups, el]
                                            :
                                                choosedGroups
                                    )
                                }}
                            />

                            <Text style={{ marginLeft: 2 }}>{ el }</Text>
                        </Card.Content>
                    </Card>
                )}
            </View>

            <Button
                mode="contained"
                style={{
                    marginLeft: "auto",
                    marginRight: "auto",
                }}

                disabled={choosedGroups.length < 2}
                onPress={() => {
                    choosedGroups.length === 2 &&
                        socket.emit("room.investigatorPlayed", { roomID: room.id, choosedGroups })
                }}
            >
                Valider
            </Button>
        </View>
    )

}
