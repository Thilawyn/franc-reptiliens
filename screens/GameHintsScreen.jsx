import { useNavigation } from "@react-navigation/native"
import React from "react"
import { StyleSheet, View, Image } from "react-native"
import { Avatar, Button, Card, Paragraph, Text, Title } from "react-native-paper"
import UserBar from "../components/user/UserBar"
import { useUser } from "../contexts/UserContext"
import { Dimensions } from "react-native"

//sont stocké dans le JSON l'élément de complot, ainsi que les indices qui lui sont lié

//import * as initiators from '../JSON/conspiracyInitiator.JSON'
//import {conspiracyInitiator} from '../JSON/conspiracyInitiator.JSON'
//import conspiracyInitiator from '../JSON/conspiracyInitiator.JSON'

const {width, height} = Dimensions.get('window')
//const hidalgo = conspiracyInitiator.conspirators[0]

//const conspiracyInitiator = require('../JSON/conspiracyInitiator.JSON')

const styles = StyleSheet.create({
    view: {
        padding: 20,
    },

    card: {
        marginBottom: 10,
    },
    hintList: {
        flexDirection: "column",
        alignSelf: "center",
    },
    hintRow: {
        flexDirection: "row",
        alignSelf: "flex-start",
        flexWrap: "wrap",
    },
    hint: {
        //width: width/2.7,
        flexWrap: "wrap",
        margin:5,
    },
    hintImage: {
        alignSelf: "center",
        width: width/3.7,
        height: width/3.7,
    },
})


export default function GameHintsScreen(props) {

    const navigation = useNavigation()

    const [user, setUser] = useUser()

    const imagePath1 = '../img/anneHidalgo.jpg'
    const imagePath2 = '../img/illuminati.jpg'
    
    const nom1 = 'Anne Hidalgo'
    const nom2 = 'Francs Reptiliens'

    return <>
        <UserBar />

        <View style={styles.view}>
            <Card
                style={styles.card}
                elevation={4}
            >
                <Card.Title
                    title="Mode Indices"
                />

                <Card.Content>
                    <Paragraph>
                        Décrivez ce que vous voyez à votre partenaire.
                    </Paragraph>
                    <View style={styles.hintList}>
                        <View style={styles.hintRow}>
                            <Card style={styles.hint}>
                                <Card.Content>
                                {/*a changer pour utilisé les données du JSON en fonction de l'état du jeu
                                juste mettre le chemin de l'image et une variable pour le nom de l'image*/}
                                    <Image 
                                        source={require(imagePath1)}
                                        style={styles.hintImage}/>
                                    <Text>
                                        {nom1}
                                    </Text>
                                </Card.Content>
                            </Card>

                            <Card style={styles.hint}>
                                <Card.Content>
                                    <Image 
                                        source={require(imagePath2)}
                                        style={styles.hintImage}/>
                                    <Text>
                                        {nom2}
                                    </Text>
                                </Card.Content>
                            </Card>
                        </View>
                        <View style={styles.hintRow}>
                            <Card style={styles.hint}>
                                <Card.Content>
                                    <Image 
                                        source={require(imagePath1)}
                                        style={styles.hintImage}/>
                                    <Text>
                                        {nom1}
                                    </Text>
                                </Card.Content>
                            </Card>

                            <Card style={styles.hint}>
                                <Card.Content>
                                    <Image 
                                        source={require(imagePath2)}
                                        style={styles.hintImage}/>
                                    <Text>
                                        {nom2}
                                    </Text>
                                </Card.Content>
                            </Card>
                        </View>
                    </View>
                </Card.Content>
            </Card>
        </View>
    </>

}
