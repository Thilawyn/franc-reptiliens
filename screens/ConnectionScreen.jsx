import { useNavigation } from "@react-navigation/native"
import React, { useEffect, useState } from "react"
import { StyleSheet, View } from "react-native"
import { Button, Snackbar, TextInput } from "react-native-paper"
import { io } from "socket.io-client"
import { useSocket } from "../contexts/SocketContext"


const styles = StyleSheet.create({
    main: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",

        padding: 20,
    },

    row: {
        marginTop: 10,
        marginBottom: 10,
    },

    fields: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },

    field: {
        marginLeft: 5,
        marginRight: 5,
    },
})


export default function ConnectionScreen(props) {

    const navigation = useNavigation()


    const [socket, setSocket] = useSocket()
    const [error, setError] = useState(null)

    useEffect(() => {
        if (!socket) return


        function onConnect() {
            console.log("Ouient")
            navigation.navigate("Home")
        }
        socket.on("connect", onConnect)

        function onConnectError(error) {
            setError("Connexion impossible : " + error.message)
        }
        socket.on("connect_error", onConnectError)


        return () => {
            socket.off("connect", onConnect)
            socket.off("connect_error", onConnectError)
        }
    }, [socket])


    const [host, setHost] = useState("http://192.168.0.0")
    const [port, setPort] = useState("8033")

    function connect() {
        setSocket(
            io(host + ":" + port, {
                transports: ["websocket"],
                reconnectionAttempts: 3,
            })
        )
    }


    return <>
        <View style={styles.main}>
            <View style={[styles.row, styles.fields]}>
                <TextInput
                    style={styles.field}

                    label="Adresse du serveur"
                    value={host}
                    onChangeText={host => setHost(host)}
                />

                <TextInput
                    style={[styles.field, { width: "25%" }]}

                    label="Port"
                    value={port}
                    onChangeText={port => setPort(port)}
                />
            </View>

            <Button
                style={styles.row}

                mode="text"
                onPress={connect}
            >
                Connexion
            </Button>
        </View>

        <Snackbar
            visible={Boolean(error)}
            onDismiss={() => setError(null)}
            duration={3000}
        >
            {error}
        </Snackbar>
    </>

}
