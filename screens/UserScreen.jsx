import { useNavigation, useRoute } from "@react-navigation/native"
import React, { useState } from "react"
import { StyleSheet, View } from "react-native"
import { Button, Snackbar, TextInput } from "react-native-paper"
import uuid from "react-native-uuid"
import { useUser } from "../contexts/UserContext"


const styles = StyleSheet.create({
    main: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "stretch",

        padding: 20,
    },

    camera: {
        height: "150px",
    },

    row: {
        marginTop: 10,
        marginBottom: 10,
    },

    buttons: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },

    button: {
        marginLeft: 5,
        marginRight: 5,
    },
})


export default function UserScreen(props) {

    const route = useRoute()
    const navigation = useNavigation()


    const [user, setUser] = useUser()
    const [userID, setUserID] = useState((user && user.id) || uuid.v4())
    const [userName, setUserName] = useState(user ? user.name : "")
    const [userPhoto, setUserPhoto] = useState(user ? user.photo : null)

    const [error, setError] = useState(null)


    // const cameraRef = useRef(null)
    // const [hasCameraPermission, setHasCameraPermission] = useState(null)

    // useEffect(() => {
    //     (async () => {
    //         const { status } = await Camera.requestCameraPermissionsAsync()
    //         setHasCameraPermission(status === "granted")
    //     })()

    // }, [])


    return <>
        <View style={styles.main}>
            <TextInput
                style={styles.row}

                label="Pseudonyme"
                value={userName}
                onChangeText={text => setUserName(text)}
            />

            {/* <Button
                style={styles.row}
                icon="camera"
                mode="contained"

                onPress={() => {}}
            >
                Votre photo
            </Button> */}

            {/* {hasCameraPermission &&
                <Camera
                    style={styles.camera}

                    ref={cameraRef}
                    type={Camera.Constants.Type.front}
                    pictureSize="4:3"
                />
            } */}

            <View style={[styles.row, styles.buttons]}>
                <Button
                    style={styles.button}

                    mode="contained"
                    onPress={() => {
                        const name = userName.trim()

                        if (name.length < 3) {
                            setError("Votre pseudonyme doit au moins faire trois caractères !")
                            return
                        }

                        setUser({
                            ...user,
                            id: userID,
                            name,
                        })

                        if (route.params && route.params.goTo)
                            navigation.replace(route.params.goTo)
                        else
                            navigation.goBack()
                    }}
                >
                    Ok
                </Button>

                <Button
                    style={styles.button}

                    mode="text"
                    onPress={() => navigation.goBack()}
                >
                    Annuler
                </Button>
            </View>
        </View>

        <Snackbar
            visible={Boolean(error)}
            onDismiss={() => setError(null)}
            duration={3000}
        >
            {error}
        </Snackbar>
    </>

}
