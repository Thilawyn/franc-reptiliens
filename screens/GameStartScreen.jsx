import { useNavigation } from "@react-navigation/native"
import React from "react"
import { StyleSheet, View } from "react-native"
import { Avatar, Button, Card, Paragraph } from "react-native-paper"
import { useRoom } from "../contexts/RoomContext"
import { useUser } from "../contexts/UserContext"


const styles = StyleSheet.create({
    view: {
        padding: 20,
    },
})


export default function GameStartScreen(props) {

    const navigation = useNavigation()

    const [ user ] = useUser()
    const [ room ] = useRoom()

    const isLeader = room.leader.id === user.id


    return (
        <View style={styles.view}>
            <Card elevation={4}>
                <Card.Title
                    title={"Bonjour " + user.name}
                    subtitle={isLeader ? "Hacker" : "Investigateur"}
                    left={props => <Avatar.Icon {...props} icon="emoticon-cool" />}
                />

                <Card.Content>
                    {isLeader ? <>
                        <Paragraph>Vous êtes le célèbre hacker { user.name }. Vous avez obtenu un mastère dans une école non reconnue par l'état et vous êtes persuadés que votre échec dans vos études est dû à un complot du gouvernement.</Paragraph>

                        <Paragraph>
                            Grâce à vos talents de 3l1t Haxx0r, vous avez réussi à entrer dans les serveurs de la DGSE et à établir une liste de personnalités ayant contacté l'organisation.
                            Un détective de l'ONU est entré en contact avec vous dans le but de déjouer un complot mondial aux conséquences incalculables.
                        </Paragraph>

                        <Paragraph>Votre mission, si vous l'acceptez, est de partager vos trouvailles avec l'investigateur.</Paragraph>
                        <Paragraph>Cependant, pour des raisons de sécurité, vous ne pouvez entrer en contact avec lui que via une connexion téléphonique encryptée à faible débit. Vous allez donc devoir décrire vos indices.</Paragraph>
                    </> : <>
                        <Paragraph>Vous êtes { user.name }, détective pour le compte de l'ONU.</Paragraph>

                        <Paragraph>
                            Au fur et à mesure des années, votre esprit affuté a remarqué des irrégularités et anormalités de plus en plus curieuses.
                            Quand vos sources les plus fiables furent brutalement assassinées, vous décidez de prendre des mesures drastiques, et d'entrer en contact avec un hacker de renom.
                        </Paragraph>
                        <Paragraph>
                            Votre objectif sera d'exploiter les indices qu'il vous transmettra depuis les serveurs de la DGSE et de découvrir quel complot se trame.
                        </Paragraph>
                        <Paragraph>
                            Malheureusement, il n'y a aucun moyen d'établir une connexion fiable et sécurisé entre vous et lui, il devra donc vous transmettre les informations de vive voix via téléphone faible débit.
                        </Paragraph>
                    </>}
                </Card.Content>

                <Card.Actions>
                    <Button
                        mode="contained"
                        onPress={() => {
                            navigation.navigate("Game")
                        }}
                    >
                        Ok
                    </Button>
                </Card.Actions>
            </Card>
        </View>
    )

}
