import { NavigationContainer } from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import React, { useState } from "react"
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper"
import NavBar from "./components/NavBar"
import { RoomProvider } from "./contexts/RoomContext"
import { SocketProvider } from "./contexts/SocketContext"
import UserContext from "./contexts/UserContext"
import ConnectionScreen from "./screens/ConnectionScreen"
import GameEndScreen from "./screens/GameEndScreen"
import GameScreen from "./screens/GameScreen"
import GameStartScreen from "./screens/GameStartScreen"
import HomeScreen from "./screens/HomeScreen"
import RoomScreen from "./screens/RoomScreen"
import UserScreen from "./screens/UserScreen"


const theme = {
    ...DefaultTheme,

    roundness: 18,
    colors: {
        ...DefaultTheme.colors,

        primary: "#F25C54",
        accent: "#F4845F",
    },
}


const Stack = createNativeStackNavigator()

export default function App() {

    const userContext = useState(null)


    return (
        <PaperProvider theme={theme}>
            <SocketProvider>
                <UserContext.Provider value={userContext}>
                    <RoomProvider>

                        <NavigationContainer>
                            <Stack.Navigator
                                initialRouteName="Connection"
                                screenOptions={{
                                    header: NavBar,
                                }}
                            >

                                <Stack.Screen name="Connection" component={ConnectionScreen} />

                                <Stack.Screen name="Home" component={HomeScreen} />
                                <Stack.Screen name="User" component={UserScreen} />
                                <Stack.Screen name="Room" component={RoomScreen} />

                                <Stack.Screen name="GameStart" component={GameStartScreen} />
                                <Stack.Screen name="Game" component={GameScreen} />

                                <Stack.Screen name="GameEnd" component={GameEndScreen} />

                            </Stack.Navigator>
                        </NavigationContainer>

                    </RoomProvider>
                </UserContext.Provider>
            </SocketProvider>
        </PaperProvider>
    )

}
