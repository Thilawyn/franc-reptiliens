import { useNavigation } from "@react-navigation/native"
import React from "react"
import { Appbar } from "react-native-paper"


export default function NavBar(props) {

    const {
        title,
    } = props

    const navigation = useNavigation()


    return (
        <Appbar.Header>
            {navigation.canGoBack && <Appbar.BackAction onPress={() => navigation.goBack()} />}
            <Appbar.Content title={title || "La Menace des Francs reptiliens"} />
        </Appbar.Header>
    )

}
