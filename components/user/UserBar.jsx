import { useNavigation } from "@react-navigation/native"
import React from "react"
import { StyleSheet, View } from "react-native"
import { FAB } from "react-native-paper"
import { useUser } from "../../contexts/UserContext"


const styles = StyleSheet.create({
    view: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",

        margin: 20,
    },
})


export default function UserBar(props) {

    const navigation = useNavigation()

    const [user, setUser] = useUser()


    return (
        <View style={styles.view}>
            <FAB
                label={user && user.name}
                icon="account"
                onPress={() => navigation.navigate("User")}
            />
        </View>
    )

}
