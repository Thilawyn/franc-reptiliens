import React, { createContext, useContext, useEffect, useState } from "react"


const SocketContext = createContext(null)
export default SocketContext


export function useSocket(props) {
    return useContext(SocketContext)
}


export function SocketProvider({ children }) {

    const socketState = useState(null)
    const [socket] = socketState

    useEffect(() => {
        if (!socket) return

        function onConnect() {
            console.log("Connected to server!")
        }
        socket.on("connect", onConnect)

        return () => {
            socket.off("connect", onConnect)
        }
    }, [socket])


    return (
        <SocketContext.Provider value={socketState}>
            { children }
        </SocketContext.Provider>
    )

}
