import { createContext, useContext } from "react"


const UserContext = createContext(null)
export default UserContext


export function useUser(props) {
    return useContext(UserContext)
}
