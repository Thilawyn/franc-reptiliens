import React, { createContext, useContext, useEffect, useState } from "react"
import { useSocket } from "./SocketContext"


const RoomContext = createContext(null)
export default RoomContext


export function useRoom(props) {
    return useContext(RoomContext)
}


export function RoomProvider(props) {

    const { children } = props

    const roomState = useState(null)
    const [, setRoom] = roomState


    const [socket] = useSocket()

    useEffect(() => {
        if (!socket) return


        function onRoomUpdate(data) {
            setRoom(data.room)
        }
        socket.on("room.update", onRoomUpdate)

        function onRoomClosed() {
            setRoom(null)
        }
        socket.on("room.closed", onRoomClosed)


        return () => {
            socket.off("room.update", onRoomUpdate)
            socket.off("room.closed", onRoomClosed)
        }
    }, [socket])


    return (
        <RoomContext.Provider value={roomState}>
            { children }
        </RoomContext.Provider>
    )

}
